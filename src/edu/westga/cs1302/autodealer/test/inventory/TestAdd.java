package edu.westga.cs1302.autodealer.test.inventory;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

import edu.westga.cs1302.autodealer.model.Automobile;
import edu.westga.cs1302.autodealer.model.Inventory;

class TestAdd {

	@Test
	void testAddNullAuto() {
		Inventory inventory = new Inventory();
		assertThrows(IllegalArgumentException.class, () -> inventory.add(null));
	}
	
	@Test
	void testAddAutoToEmptyInventory() {
		Inventory inventory = new Inventory();
		boolean added = inventory.add(new Automobile("Ford", "Focus", 2008, 108132.2, 6900));
		assertEquals(1, inventory.size());
		assertTrue(added);
	}
	
	@Test
	void testAddMultiplesAutoToInventory() {
		Inventory inventory = new Inventory();
		inventory.add(new Automobile("Ford", "Focus", 2008, 108132.2, 6900));
		boolean added = inventory.add(new Automobile("Ford", "Focus", 2007, 108132.2, 6900));
		assertEquals(2, inventory.size());
		assertTrue(added);
	}
	@Test
	void testAddMultiplesAutoToInventoryTwoAreSame() {
		Inventory inventory = new Inventory();
		inventory.add(new Automobile("Ford", "Focus", 2008, 108132.2, 6900));
		boolean added1 = inventory.add(new Automobile("Ford", "F150", 2008, 108132.2, 6900));
		boolean added2 = inventory.add(new Automobile("Ford", "Mustang", 2008, 108132.2, 6900));
		boolean added3 = inventory.add(new Automobile("Ford", "Focus", 2008, 108132.2, 6900));
		boolean added4 = inventory.add(new Automobile("Ford", "Fusion", 2008, 108132.2, 6900));
		assertEquals(4, inventory.size());
		assertTrue(added1);
		assertTrue(added2);
		assertFalse(added3);
		assertTrue(added4);
	}

}