package edu.westga.cs1302.autodealer.test.inventory;

import static org.junit.jupiter.api.Assertions.assertEquals;
import java.util.ArrayList;

import org.junit.jupiter.api.Test;

import edu.westga.cs1302.autodealer.model.Automobile;
import edu.westga.cs1302.autodealer.model.Inventory;

public class TestAutosByMake {
	
	
	@Test
	void testNoneMathDesiredMakeFilter() {
		Inventory inventory = new Inventory();
		inventory.add(new Automobile("Ford", "Focus", 2008, 108132.2, 6900));
		inventory.add(new Automobile("Ford", "fud", 2008, 108132.2, 6900));
		inventory.add(new Automobile("Ford", "f150", 2008, 108132.2, 6900));
		inventory.add(new Automobile("Ford", "mustang", 2008, 108132.2, 6900));
		inventory.add(new Automobile("Chevy", "mustang", 2008, 108132.2, 6900));
		ArrayList<Automobile> carsByMake = inventory.autosByMake("Nissan");
		assertEquals(0, carsByMake.size());

	}
	
	@Test
	void testOneSameMake() {
		Inventory inventory = new Inventory();
		inventory.add(new Automobile("Ford", "Focus", 2008, 108132.2, 6900));
		inventory.add(new Automobile("Ford", "fud", 2008, 108132.2, 6900));
		inventory.add(new Automobile("Ford", "f150", 2008, 108132.2, 6900));
		inventory.add(new Automobile("Ford", "mustang", 2008, 108132.2, 6900));
		inventory.add(new Automobile("Chevy", "mustang", 2008, 108132.2, 6900));
		ArrayList<Automobile> carsByMake = inventory.autosByMake("Chevy");
		assertEquals(1, carsByMake.size());

	}
	
	@Test
	void testSeveralSameMake() {
		Inventory inventory = new Inventory();
		inventory.add(new Automobile("Ford", "Focus", 2008, 108132.2, 6900));
		inventory.add(new Automobile("Ford", "fud", 2008, 108132.2, 6900));
		inventory.add(new Automobile("Ford", "f150", 2008, 108132.2, 6900));
		inventory.add(new Automobile("Ford", "mustang", 2008, 108132.2, 6900));
		inventory.add(new Automobile("Chevy", "mustang", 2008, 108132.2, 6900));
		ArrayList<Automobile> carsByMake = inventory.autosByMake("Ford");
		assertEquals(4, carsByMake.size());

	}
	
	

}
