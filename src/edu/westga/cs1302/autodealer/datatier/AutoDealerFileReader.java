package edu.westga.cs1302.autodealer.datatier;

import java.util.logging.Logger;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Scanner;
import java.util.logging.FileHandler;
import java.util.logging.SimpleFormatter;

import edu.westga.cs1302.autodealer.model.Automobile;
import edu.westga.cs1302.autodealer.model.Dealership;
import edu.westga.cs1302.autodealer.model.DealershipGroup;
import edu.westga.cs1302.autodealer.resources.UI;

/**
 * The Class AutoDealerFileReader. Reads an .adi (Auto Dealer Inventory) file
 * which is a CSV file with the following format: make,model,year,miles,price
 * 
 * @author CS1302
 */
public class AutoDealerFileReader {
	public static final String FIELD_SEPARATOR = ",";

	private File inventoryFile;

	/**
	 * Instantiates a new auto dealer file reader.
	 *
	 * @precondition inventoryFile != null
	 * @postcondition none
	 * 
	 * @param inventoryFile the inventory file
	 */
	public AutoDealerFileReader(File inventoryFile) {
		if (inventoryFile == null) {
			throw new IllegalArgumentException(UI.ExceptionMessages.INVENTORY_FILE_CANNOT_BE_NULL);
		}

		this.inventoryFile = inventoryFile;
	}

	/**
	 * Opens the associated adi file and reads all the autos in the file one line at
	 * a time. Parses each line and creates an automobile object and stores it in an
	 * ArrayList of Automobile objects. Once the file has been completely read the
	 * ArrayList of Automobiles is returned from the method. Assumes all autos in
	 * the file are for the same dealership.
	 * 
	 * @precondition none
	 * @postcondition none
	 * 
	 * @return Collection of Automobile objects read from the file.
	 * @throws FileNotFoundException File not found.
	 */
	public ArrayList<Automobile> loadAllAutos() throws FileNotFoundException {
		ArrayList<Automobile> autos = new ArrayList<Automobile>();
		String automobileMissing = "";
		try (Scanner input = new Scanner(this.inventoryFile)) {
			while (input.hasNextLine()) {
				automobileMissing = "";
				String line = input.nextLine();
				String[] fields = this.splitLine(line, AutoDealerFileReader.FIELD_SEPARATOR);

				automobileMissing = this.forLoopForAutomobileMissing(automobileMissing, fields);
				try {
					Automobile auto = this.makeAutomobile(fields);
					autos.add(auto);
				} catch (Exception e) {
					System.err.println("Error reading file: empty String on line: " + automobileMissing);
				}
			}

			return autos;
		}
	}

	/**
	 * Load all the autos in the file into the dealership group placing the autos in
	 * the appropriate dealership
	 * 
	 * @param dealershipGroup the arraylist of dealers
	 * @throws FileNotFoundException file not found
	 * 
	 */

	public void loadAutosIntoCorrectDealership(DealershipGroup dealershipGroup) throws FileNotFoundException {
		ArrayList<Automobile> autos = new ArrayList<Automobile>();
		String automobileMissing = "";
		try (Scanner input = new Scanner(this.inventoryFile)) {
			while (input.hasNextLine()) {
				automobileMissing = "";
				String line = input.nextLine();
				String[] fields = this.splitLine(line, AutoDealerFileReader.FIELD_SEPARATOR);
				String dealerName = fields[0];

				int dealerShipExistsCount = 0;
				for (Dealership currdealership : dealershipGroup.getDealers()) {
					if (currdealership.getName().equals(dealerName)) {
						dealerShipExistsCount++;
					}

				}
				if (dealerShipExistsCount == 0) {
					dealershipGroup.addDealership(dealerName);
				}

				automobileMissing = this.forLoopForAutomobileMissing(automobileMissing, fields);
				try {
					Automobile auto = this.makeAutomobile(fields);
					autos.add(auto);
					dealershipGroup.addAuto(DealershipGroup.ALL_AUTOS, auto);
					dealershipGroup.addAuto(dealerName, auto);
				} catch (Exception e) {
					this.writeErrorFile();
				}
			}

		}

	}

	private void writeErrorFile() {
		Logger logger = Logger.getLogger("MyLog"); 
		FileHandler fh;
		try {
			fh = new FileHandler("%h/readErrors.log", true);
			logger.addHandler(fh);
			SimpleFormatter formatter = new SimpleFormatter();
			fh.setFormatter(formatter);
			
			logger.info(UI.Text.FILE_OPEN_ERROR);
		} catch (SecurityException | IOException e) {
			e.printStackTrace();
		}
	}

	/**
	 * @param automobileMissing
	 * @param fields
	 * @return
	 */
	private String forLoopForAutomobileMissing(String automobileMissing, String[] fields) {
		for (int i = 0; i < fields.length; i++) {
			if (fields[i].equals("")) {
				automobileMissing += AutoDealerFileReader.FIELD_SEPARATOR;
			} else if (i == fields.length) {
				automobileMissing += fields[i];
			} else {
				automobileMissing += fields[i] + AutoDealerFileReader.FIELD_SEPARATOR;
			}

		}
		return automobileMissing;
	}

	private Automobile makeAutomobile(String[] fields) {
		String make = fields[1];
		String model = fields[2];
		int year = Integer.parseInt(fields[3]);
		double miles = Double.parseDouble(fields[4]);
		double price = Double.parseDouble(fields[5]);
		return new Automobile(make, model, year, miles, price);
	}

	private String[] splitLine(String line, String fieldSeparator) {
		return line.split(fieldSeparator);
	}

}
