package edu.westga.cs1302.autodealer.datatier;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.ArrayList;
import edu.westga.cs1302.autodealer.model.Automobile;
import edu.westga.cs1302.autodealer.model.Dealership;
import edu.westga.cs1302.autodealer.model.DealershipGroup;
import edu.westga.cs1302.autodealer.resources.UI;

/**
 * The Class AlbumFileWriter.
 * 
 * @author CS1302
 */
public class AutoDealerFileWriter {

	private File inventoryFile;

	/**
	 * Instantiates a new auto file writer.
	 *
	 * @precondition inventoryFile != null
	 * @postcondition none
	 * 
	 * @param inventoryFile the inventory file
	 */
	public AutoDealerFileWriter(File inventoryFile) {
		if (inventoryFile == null) {
			throw new IllegalArgumentException(UI.ExceptionMessages.INVENTORY_FILE_CANNOT_BE_NULL);
		}

		this.inventoryFile = inventoryFile;
	}

	/**
	 * Writes all the autos in the dealership to the specified auto file. Each auto
	 * will be on a separate line and of the following format:
	 * make,model,year,miles,price
	 * 
	 * @precondition autos != null
	 * @postcondition none
	 * 
	 * @param autos The collection of autos to write to file.
	 * @throws FileNotFoundException 
	 */
	public void write(ArrayList<Automobile> autos) throws FileNotFoundException {
		if (autos == null) {
			throw new IllegalArgumentException(UI.ExceptionMessages.AUTOS_CANNOT_BE_NULL);
		}

		try (PrintWriter writer = new PrintWriter(this.inventoryFile)) {
			for (Automobile currAuto : autos) {
				String output = currAuto.getMake() + AutoDealerFileReader.FIELD_SEPARATOR;
				output += currAuto.getModel() + AutoDealerFileReader.FIELD_SEPARATOR;
				output += currAuto.getYear() + AutoDealerFileReader.FIELD_SEPARATOR;
				output += currAuto.getMiles() + AutoDealerFileReader.FIELD_SEPARATOR;
				output += currAuto.getPrice();
				
				writer.println(output);
			}
		}

	}
	
	/**
	 * 
	 * Writes dealerships
	 * 
	 * @precondition autos != null
	 * @postcondition none
	 * 
	 * @param dealers The collection of dealerships to write to file.
	 * @throws FileNotFoundException 
	 */
	
	public void writeDealerShips(DealershipGroup dealers) throws FileNotFoundException {
		if (dealers == null) {
			throw new IllegalArgumentException(UI.ExceptionMessages.DEALERS_CANNOT_BE_NULL);
		}

		try (PrintWriter writer = new PrintWriter(this.inventoryFile)) {
			for (Dealership currDealer : dealers.getDealers()) {
				if (!currDealer.getName().equals(DealershipGroup.ALL_AUTOS)) {
					for (Automobile currAuto : currDealer.getInventory().getAutos()) {
						String output = currDealer.getName() + AutoDealerFileReader.FIELD_SEPARATOR;
						output += currAuto.getMake() + AutoDealerFileReader.FIELD_SEPARATOR;
						output += currAuto.getModel() + AutoDealerFileReader.FIELD_SEPARATOR;
						output += currAuto.getYear() + AutoDealerFileReader.FIELD_SEPARATOR;
						output += currAuto.getMiles() + AutoDealerFileReader.FIELD_SEPARATOR;
						output += currAuto.getPrice();
						writer.println(output);
					}
				}
			}
		}

	}

}
