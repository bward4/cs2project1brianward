package edu.westga.cs1302.autodealer.model;

import java.util.ArrayList;

import edu.westga.cs1302.autodealer.resources.UI;

/**
 * The Class DealershipGroup - store a collection of dealerships
 * 
 * @author CS1302
 */
public class DealershipGroup {
	public static final String ALL_AUTOS = "ALL AUTOS";

	private ArrayList<Dealership> dealers;

	/**
	 * Instantiates a new dealership group.
	 */
	public DealershipGroup() {
		this.dealers = new ArrayList<Dealership>();
		this.addDealership(ALL_AUTOS);
	}

	/**
	 * Adds the dealership.
	 * 
	 * @precondition name not null; name not empty
	 * @postcondition if new dealership then adds new dealership 
	 *
	 * @param name the name
	 * @return true, if adding new dealership, false if dealership already exists
	 */
	public boolean addDealership(String name) {
		if (name == null) {
			throw new IllegalArgumentException(UI.ExceptionMessages.NAME_CANNOT_BE_NULL);
		}
		
		if (name.isEmpty()) {
			throw new IllegalArgumentException(UI.ExceptionMessages.NAME_CANNOT_BE_EMPTY);
		}

		for (Dealership currDealer : this.dealers) {
			if (name.equalsIgnoreCase(currDealer.getName())) {
				return false;
			}
		}

		Dealership dealer = new Dealership(name);
		return this.dealers.add(dealer);
	}

	/**
	 * Finds dealership with specified name
	 *
	 * @param name the name 
	 * @return the dealership if found, null otherwise
	 */
	public Dealership findDealership(String name) {
		if (name == null) {
			throw new IllegalArgumentException(UI.ExceptionMessages.NAME_CANNOT_BE_NULL);
		}

		for (Dealership currDealer : this.dealers) {
			if (name.equalsIgnoreCase(currDealer.getName())) {
				return currDealer;
			}
		}

		return null;
	}

	/**
	 * Adds the specified auto the the specified dealer
	 * 
	 * @precondition dealerName not null; auto not null
	 * @postconditon auto added to specified dealership 
	 *
	 * @param dealerName the dealer name
	 * @param auto the auto
	 * @return true, if auto successfully added; false, otherwise
	 */
	public boolean addAuto(String dealerName, Automobile auto) {
		if (dealerName == null) {
			throw new IllegalArgumentException(UI.ExceptionMessages.DEALERNAME_CANNOT_BE_NULL);
		}

		if (auto == null) {
			throw new IllegalArgumentException(UI.ExceptionMessages.AUTO_CANNOT_BE_NULL);
		}
		
		Dealership dealer = this.findDealership(dealerName);
		if (dealer != null) {
			return dealer.getInventory().add(auto);
		}

		return false;
	}

	/**
	 * Removes the specified auto from the specified dealer name.
	 * 
	 * @precondition dealerName != null; auto != null
	 * @postcondition if auto found at dealer, the inventory size is for that dealer
	 *               is reduced by one
	 *
	 * @param dealerName the dealer name
	 * @param auto       the auto
	 * @return true, if successful
	 */
	public boolean removeAuto(String dealerName, Automobile auto) {
		if (dealerName == null) {
			throw new IllegalArgumentException(UI.ExceptionMessages.DEALERNAME_CANNOT_BE_NULL);
		}

		if (auto == null) {
			throw new IllegalArgumentException(UI.ExceptionMessages.AUTO_CANNOT_BE_NULL);
		}

		Dealership dealer = this.findDealership(dealerName);
		if (dealer != null) {
			return dealer.getInventory().remove(auto);
		}

		return false;
	}

	/**
	 * Gets the dealers.
	 * 
	 * @precondition none
	 * @postcondition none
	 *
	 * @return the dealers
	 */
	public ArrayList<Dealership> getDealers() {
		return this.dealers;
	}

}
